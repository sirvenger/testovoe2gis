<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191110091704 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE building (id INT AUTO_INCREMENT NOT NULL, address VARCHAR(255) NOT NULL, lat DOUBLE PRECISION NOT NULL, lng DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE heading (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_2B43FC1727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firm (id INT AUTO_INCREMENT NOT NULL, building_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_560581FD4D2A7E12 (building_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE firms_rubricks (firm_id INT NOT NULL, headings_id INT NOT NULL, INDEX IDX_1AD7320389AF7860 (firm_id), INDEX IDX_1AD732034EAFED12 (headings_id), PRIMARY KEY(firm_id, headings_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE heading ADD CONSTRAINT FK_2B43FC1727ACA70 FOREIGN KEY (parent_id) REFERENCES heading (id)');
        $this->addSql('ALTER TABLE firm ADD CONSTRAINT FK_560581FD4D2A7E12 FOREIGN KEY (building_id) REFERENCES building (id)');
        $this->addSql('ALTER TABLE firms_rubricks ADD CONSTRAINT FK_1AD7320389AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id)');
        $this->addSql('ALTER TABLE firms_rubricks ADD CONSTRAINT FK_1AD732034EAFED12 FOREIGN KEY (headings_id) REFERENCES heading (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE firm DROP FOREIGN KEY FK_560581FD4D2A7E12');
        $this->addSql('ALTER TABLE heading DROP FOREIGN KEY FK_2B43FC1727ACA70');
        $this->addSql('ALTER TABLE firms_rubricks DROP FOREIGN KEY FK_1AD732034EAFED12');
        $this->addSql('ALTER TABLE firms_rubricks DROP FOREIGN KEY FK_1AD7320389AF7860');
        $this->addSql('DROP TABLE building');
        $this->addSql('DROP TABLE heading');
        $this->addSql('DROP TABLE firm');
        $this->addSql('DROP TABLE firms_rubricks');
    }
}
