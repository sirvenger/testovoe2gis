<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191112145320 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE firms_headings (firm_id INT NOT NULL, headings_id INT NOT NULL, INDEX IDX_5D2B4FFD89AF7860 (firm_id), INDEX IDX_5D2B4FFD4EAFED12 (headings_id), PRIMARY KEY(firm_id, headings_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE firms_headings ADD CONSTRAINT FK_5D2B4FFD89AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id)');
        $this->addSql('ALTER TABLE firms_headings ADD CONSTRAINT FK_5D2B4FFD4EAFED12 FOREIGN KEY (headings_id) REFERENCES heading (id)');
        $this->addSql('DROP TABLE firms_rubricks');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE firms_rubricks (firm_id INT NOT NULL, headings_id INT NOT NULL, INDEX IDX_1AD732034EAFED12 (headings_id), INDEX IDX_1AD7320389AF7860 (firm_id), PRIMARY KEY(firm_id, headings_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE firms_rubricks ADD CONSTRAINT FK_1AD732034EAFED12 FOREIGN KEY (headings_id) REFERENCES heading (id)');
        $this->addSql('ALTER TABLE firms_rubricks ADD CONSTRAINT FK_1AD7320389AF7860 FOREIGN KEY (firm_id) REFERENCES firm (id)');
        $this->addSql('DROP TABLE firms_headings');
    }
}
