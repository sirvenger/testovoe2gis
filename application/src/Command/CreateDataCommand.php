<?php

namespace App\Command;

use App\Entity\Building;
use App\Entity\Heading;
use App\Repository\BuildingRepository;
use App\Repository\FirmRepository;
use App\Repository\HeadingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Firm;

class CreateDataCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:create-data';

    private $firmRepository;

    private $headingRepository;

    private $buildingRepository;

    private $manager;

    public function __construct(
        EntityManagerInterface $manager,
        FirmRepository $firmRepository,
        HeadingRepository $headingRepository,
        BuildingRepository $buildingRepository
    ) {
        $this->firmRepository = $firmRepository;
        $this->headingRepository = $headingRepository;
        $this->buildingRepository = $buildingRepository;
        $this->manager = $manager;

        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('count', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '-1');
        $count = $input->getArgument('count');
        for ($i = 0; $i < $count; $i++) {
            $firm = new Firm();
            $firm->setName('Address '. $i * $count);
            $firm->setNumbers('+7 4242 267' . $i . ' 426-312' .$i * $count);
            $firm->setBuilding($this->manager->getReference(Building::class, (mt_rand(1, 9999))));
            $firm->addHeading($this->manager->getReference(Heading::class, mt_rand(3, 6) * mt_rand(1, 5)));
            $this->manager->persist($firm);
            $output->writeln($i);
            if ($i % 100 === 0) {
                $this->manager->flush();
                $this->manager->clear();
            }
        }

        $output->writeln('Done');
    }
}