<?php

namespace App\DataFixtures;

use App\Entity\Building;
use App\Entity\Firm;
use App\Entity\Heading;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $buildingRepository = $manager->getRepository(Building::class);
        $headingRepository = $manager->getRepository(Heading::class);

        for ($i = 1; $i < 10000; $i++) {
            $building = new Building();
            $building->setAddress('Адрес №'. $i);
            $building->setX(mt_rand($i, 10000) / 1000);
            $building->setY(mt_rand($i, 10000) / 1000);
            $manager->persist($building);
        }

//        for ($i = 1; $i < 5; $i++) {
//            $headingFirstLevel = new Heading();
//            $headingFirstLevel->setName('Рубрика №'. $i);
//            $manager->persist($headingFirstLevel);
//            for ($j = 1; $j < 5; $j++) {
//                $headingSecondLevel = new Heading();
//                $headingSecondLevel->setName('Под Рубрика №'. '0' . $j . '0' . $i);
//                $headingSecondLevel->setParent($headingFirstLevel);
//                $manager->persist($headingSecondLevel);
//                for ($k = 1; $k < 5; $k++) {
//                    $headingThirdLevel = new Heading();
//                    $headingThirdLevel->setName('Под Рубрика №'. '0' . $j . '0' . $i . '0'. $k);
//                    $headingThirdLevel->setParent($headingSecondLevel);
//                    $manager->persist($headingThirdLevel);
//                }
//            }
//        }

        $manager->flush();
    }
}
