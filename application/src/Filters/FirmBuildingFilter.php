<?php


namespace App\Filters;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class FirmBuildingFilter extends AbstractContextAwareFilter
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface|null
     */
    private $security;


    public function __construct(ManagerRegistry $managerRegistry, ?RequestStack $requestStack, Security $security, LoggerInterface $logger = null, array $properties = null)
    {
        $this->security = $security;
        parent::__construct($managerRegistry, $requestStack, $logger, $properties);
    }

    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = []): void
    {
        parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
    }

    public function filterBuilding(string $value, QueryBuilder $queryBuilder)
    {
        if (!$value) {
            return;
        }

        $queryBuilder
            ->where($queryBuilder->getRootAliases()[0] . '.building = :id')
            ->setParameter('id', $value);
    }

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        switch ($property) {
            case 'building':
                $this->filterBuilding($value, $queryBuilder);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            'building' => [
                'property' => 'building',
                'type' => 'int',
                'required' => true,
            ]
        ];
    }
}