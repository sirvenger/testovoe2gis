<?php


namespace App\Filters;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Repository\BuildingRepository;
use App\Repository\FirmRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class FirmRadiusFilter extends AbstractContextAwareFilter
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface|null
     */
    private $security;
    /**
     * @var BuildingRepository
     */
    private $repository;

    private $point_x = 0;
    private $point_y = 0;
    private $radius = 0;

    public function __construct(ManagerRegistry $managerRegistry, ?RequestStack $requestStack, Security $security, LoggerInterface $logger = null, array $properties = null, BuildingRepository $repository)
    {
        $this->security = $security;
        $this->repository = $repository;
        parent::__construct($managerRegistry, $requestStack, $logger, $properties);
    }

    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = []): void
    {
       parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
    }

    public function filterByRadius($value, QueryBuilder $queryBuilder)
    {
        if (!$value) {
            return;
        }

        $ids = $this->repository->createQueryBuilder('b')
            ->select('b.id')
            ->where('b.x <= :x + :radius')
            ->andWhere('b.x >= :x - :radius')
            ->andWhere('b.y <= :y + :radius')
            ->andWhere('b.y >= :y - :radius')
            ->andWhere('((b.x - :x) * (b.x - :x) + (b.y - :y) * (b.y - :y)) <= :radius * :radius')
            ->setParameter('x', $this->point_x)
            ->setParameter('y', $this->point_y)
            ->setParameter('radius', $value)
            ->getQuery()
            ->getScalarResult();

        $ids = array_map('current', $ids);

        $queryBuilder
            ->add('where', $queryBuilder->expr()->in($queryBuilder->getRootAliases()[0] . '.building', $ids));
    }

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        switch ($property) {
            case 'point_x':
                $this->point_x = $value;
                break;
            case 'point_y':
                $this->point_y = $value;
                break;
            case 'radius':
                $this->filterByRadius($value, $queryBuilder);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            'point_x' => [
                'property' => 'point_x',
                'type' => 'float',
                'required' => false,
            ],
            'point_y' => [
                'property' => 'point_y',
                'type' => 'float',
                'required' => false,
            ],
            'radius' => [
                'property' => 'radius',
                'type' => 'float',
                'required' => false,
            ],
        ];
    }
}
