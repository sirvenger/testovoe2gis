<?php

namespace App\Filters;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Repository\BuildingRepository;
use App\Repository\FirmRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\Query\ResultSetMapping;

class FirmHeadingFilter extends AbstractContextAwareFilter
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface|null
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(ManagerRegistry $managerRegistry, ?RequestStack $requestStack, Security $security, LoggerInterface $logger = null, array $properties = null, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
        parent::__construct($managerRegistry, $requestStack, $logger, $properties);
    }

    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null, array $context = []): void
    {
        parent::apply($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
    }

    public function filterHeading(int $value, QueryBuilder $queryBuilder)
    {
        if (!$value) {
            return;
        }

        $sql = 'SELECT firm_id FROM firms_headings WHERE headings_id = '. $value;
        $stmt = $this->manager->getConnection()->prepare($sql);
        $stmt->execute();
        $ids = $stmt->fetchAll();

        $ids = array_map('current', $ids);

        $queryBuilder
            ->add('where', $queryBuilder->expr()->in($queryBuilder->getRootAliases()[0] . '.id', $ids));
    }

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
    {
        switch ($property) {
            case 'heading':
                $this->filterHeading($value, $queryBuilder);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            'heading' => [
                'property' => 'heading',
                'type' => 'int',
                'required' => true,
            ]
        ];
    }
}