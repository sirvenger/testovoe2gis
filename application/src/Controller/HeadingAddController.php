<?php

namespace App\Controller;

use App\Entity\ApiResource\AddFirmResource;
use App\Entity\ApiResource\AddHeadingResource;
use App\Entity\Building;
use App\Entity\Firm;
use App\Entity\Heading;
use App\Repository\HeadingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HeadingAddController extends AbstractController
{
    private $repository;
    private $entityManager;

    public function __construct(ObjectManager $manager, HeadingRepository $headingRepository)
    {
        $this->entityManager = $manager;
        $this->repository = $headingRepository;
    }

    public function __invoke(AddHeadingResource $data)
    {
        /** @var Building $building */
        $heading_parent = $this->repository->find($data->parent_id);

        $heading = new Heading();
        $heading->setName($data->name);
        if ($heading_parent) {
            $heading->setParent($heading_parent);
        }

        $this->entityManager->persist( $heading);
        $this->entityManager->flush();

        return $this->json('success');
    }
}