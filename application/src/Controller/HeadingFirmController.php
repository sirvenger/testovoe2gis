<?php

namespace App\Controller;

use App\Entity\ApiResource\AddHeadingFirmResource;
use App\Entity\ApiResource\AddHeadingResource;
use App\Entity\Building;
use App\Entity\Firm;
use App\Entity\Heading;
use App\Repository\FirmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\HeadingRepository;
use Doctrine\Common\Persistence\ObjectManager;

class HeadingFirmController extends AbstractController
{
    private $repository;
    private $entityManager;
    private $firmRepository;

    public function __construct(ObjectManager $manager, HeadingRepository $headingRepository, FirmRepository $firmRepository)
    {
        $this->entityManager = $manager;
        $this->repository = $headingRepository;
        $this->firmRepository = $firmRepository;
    }

    public function __invoke(AddHeadingFirmResource $data)
    {
        $firm = $this->firmRepository->find($data->firm_id);

        $heading = $this->repository->find($data->heading_id);

        while ($heading->getParent() !== null) {
            $firm->addHeading($heading);
            $heading = $heading->getParent();
        }
        $firm->addHeading($heading);

        $this->entityManager->persist($heading);
        $this->entityManager->flush();

        return $this->json('success');
    }
}