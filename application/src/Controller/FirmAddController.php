<?php

namespace App\Controller;

use App\Entity\ApiResource\AddFirmResource;
use App\Repository\BuildingRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Firm;
use App\Entity\Building;

class FirmAddController extends AbstractController
{
    private $repository;
    private $entityManager;

    public function __construct(ObjectManager $manager, BuildingRepository $headingRepository)
    {
        $this->entityManager = $manager;
        $this->repository = $headingRepository;
    }

    public function __invoke(AddFirmResource $data)
    {
        /** @var Building $building */
        $building = $this->repository->find($data->buildings_id);

        $firm = new Firm();
        $firm->setName($data->name);
        $firm->setNumbers($data->numbers);
        $firm->setBuilding($building);

        $this->entityManager->persist($firm);
        $this->entityManager->flush();

        return $this->json('success');
    }
}