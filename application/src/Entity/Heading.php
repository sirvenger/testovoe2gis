<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Firm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     shortName="Heading",
 *     attributes={
 *      "normalization_context"={"groups"={"headings-read"}, "enable_max_depth"="true"},
 *      "force_eager"=true
 *     },
 *     itemOperations={"get"={"method"="GET", "path"="/headings/{id}", "openapi_context" = {
 *                  "summary" = "Поиск рубрики по id"
 *                }}},
 *     collectionOperations={
 *      "get"={
 *              "method"="GET",
 *              "path"="headings",
 *              "normalization_context"={"groups"={"headings-read"}},
 *              "openapi_context" = {
 *                  "summary" = "Вывод всех рубрик"
 *                }
 *           }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\HeadingRepository")
 */
class Heading
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"headings-read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"headings-read"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Heading", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @Groups({"headings-read"})
     */
    private $parent;

    /**
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="Heading", mappedBy="parent")
     */
    private $children;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Firm", mappedBy="headings", cascade={"persist"})
     */
    private $firms;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->firms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent(Heading $parent): void
    {
        $this->parent = $parent;
    }

    public function getFirms(): ArrayCollection
    {
        return $this->firms;
    }

    public function addFirm(Firm $firm): void
    {
        if ($this->firms->contains($firm)) {
            return;
        }
        $this->firms->add($firm);
        $firm->addHeading($this);
    }

    public function removeFirm(Firm $firm): void
    {
        $this->firms->removeElement($firm);
    }
}
