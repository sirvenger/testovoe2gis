<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     shortName="Buildings",
 *     attributes={
 *      "normalization_context"={"groups"={"buildings-read"}, "enable_max_depth"="true"},
 *      "force_eager"=true
 *     },
 *     itemOperations={
 *          "get"={"method"="GET", "path"="/buildings/{id}",
 *                  "openapi_context" = {
 *                      "summary" = "Получение здания по id"
 *                }
 *          },
 *     },
 *     collectionOperations={
 *      "post"={"method"="POST", "path"="/buildings/add",
 *              "openapi_context" = {
 *                  "summary" = "Добавление здания"
 *                }
*              },
 *      "get"={
 *              "method"="GET",
 *              "path"="buildings",
 *              "normalization_context"={"groups"={"buildings-read"}},
 *              "openapi_context" = {
 *                  "summary" = "Вывод списка зданий"
 *                }
 *           }
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BuildingRepository")
 */
class Building
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"firms-read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     * @Groups({"buildings-read", "firms-read"})
     */
    private $address;

    /**
     * @ORM\Column(type="float")
     * @var float
     * @Groups({"buildings-read"})
     */
    private $x;

    /**
     * @ORM\Column(type="float")
     * @var float
     * @Groups({"buildings-read"})
     */
    private $y;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return float
     */
    public function getX(): float
    {
        return $this->x;
    }

    /**
     * @param float $x
     */
    public function setX(float $x): void
    {
        $this->x = $x;
    }

    /**
     * @return float
     */
    public function getY(): float
    {
        return $this->y;
    }

    /**
     * @param float $y
     */
    public function setY(float $y): void
    {
        $this->y = $y;
    }
}

