<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Building;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Filters\FirmHeadingFilter;
use App\Filters\FirmRadiusFilter;
use App\Filters\FirmBuildingFilter;
use App\Filters\FirmByNameFilter;

/**
 * @ApiResource(
 *     shortName="Firms",
 *     attributes={
 *      "normalization_context"={"groups"={"firms-read"}, "enable_max_depth"="true"},
 *      "force_eager"=true
 *     },
 *     itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "path"="/firms/{id}",
 *              "openapi_context" = {
 *                  "summary" = "Поиск фирмы по id"
 *                }
 *          }
 *     },
 *     collectionOperations={
 *      "get"={
 *              "method"="GET",
 *              "path"="firms",
 *              "normalization_context"={"groups"={"firms-read"}},
 *              "openapi_context" = {
 *                  "summary" = "Поиск фирм по id здания"
 *               },
 *              "filters"={FirmBuildingFilter::class},
 *              "pagination_enabled"=false
 *           },
 *     "get_by_name"={
 *              "method"="GET",
 *              "path"="firms/name",
 *              "normalization_context"={"groups"={"firms-read"}},
 *              "openapi_context" = {
 *                  "summary" = "Поиск названию фирмы"
 *               },
 *              "filters"={FirmByNameFilter::class}
 *           },
 *     "get_by_headings"={
 *          "method"="GET",
 *          "path"="/firms/heading",
 *          "normalization_context"={
 *              "groups"={"firms-read"},
 *              "enable_max_depth"=true
 *          },
 *          "openapi_context" = {
 *               "summary" = "Поиск фирм по id рубрики"
 *          },
 *          "filters"={FirmHeadingFilter::class},
 *     },
 *     "get_by_radius"={
 *          "method"="GET",
 *          "path"="/firms/radius",
 *          "normalization_context"={
 *              "groups"={"firms-read"},
 *              "enable_max_depth"=true
 *          },
 *          "openapi_context" = {
 *               "summary" = "Поиск фирм радиусе"
 *          },
 *          "pagination_items_per_page"=100,
 *          "filters"={FirmRadiusFilter::class}
 *     },
 *   }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\FirmRepository")
 */
class Firm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"firms-read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"firms-read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"firms-read"})
     */
    private $numbers;

    /**
     * @ORM\ManyToOne(targetEntity="Building", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    private $building;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Heading", inversedBy="firms", cascade={"persist","remove"}, fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="firms_headings",
     *     joinColumns={
     *      @ORM\JoinColumn(name="firm_id", referencedColumnName="id")
     *  },
     *      inverseJoinColumns={
     *      @ORM\JoinColumn(name="headings_id", referencedColumnName="id")
     *  }
     *     )
     */
    private $headings;

    public function __construct()
    {
        $this->headings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getHeadings(): Collection
    {
        return $this->headings;
    }

    public function addHeading(Heading $heading): void
    {
        if ($this->headings->contains($heading)) {
            return;
        }

        $this->headings->add($heading);
        $heading->addFirm($this);
    }

    public function removeHeading(Heading $heading): void
    {
        $this->headings->removeElement($heading);
        $heading->removeFirm($this);
    }

    /**
     * @return mixed
     */
    public function getNumbers()
    {
        return $this->numbers;
    }

    /**
     * @param mixed $numbers
     */
    public function setNumbers($numbers): void
    {
        $this->numbers = $numbers;
    }

    /**
     * @return Building
     */
    public function getBuilding(): Building
    {
        return $this->building;
    }

    /**
     * @param Building $building
     */
    public function setBuilding(Building $building): void
    {
        $this->building = $building;
    }
}
