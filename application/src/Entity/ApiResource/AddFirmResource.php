<?php

namespace App\Entity\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\FirmAddController;

/**
 * @ApiResource(
 *     shortName="Firms",
 *     routePrefix="firms",
 *     collectionOperations={
 *         "create"={
 *          "method"="POST", "path"="/add", "controller"=FirmAddController::class,
 *          "openapi_context" = {
 *                  "summary" = "Добавление фирмы"
 *                }
 *          }
 *     },
 *     itemOperations={},
 *     output=false
 * )
 */
class AddFirmResource
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $numbers;
    /**
     * @var int
     */
    public $buildings_id;
}