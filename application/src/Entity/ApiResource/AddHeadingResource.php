<?php

namespace App\Entity\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\HeadingAddController;

/**
 * @ApiResource(
 *     shortName="Heading",
 *     routePrefix="headings",
 *     collectionOperations={
 *         "create"={
 *          "method"="POST", "path"="/add", "controller"=HeadingAddController::class,
 *          "openapi_context" = {
 *                  "summary" = "Добаление рубрики"
 *                }
 *          }
 *     },
 *     itemOperations={},
 *     output=false
 * )
 */
class AddHeadingResource
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $parent_id;
}