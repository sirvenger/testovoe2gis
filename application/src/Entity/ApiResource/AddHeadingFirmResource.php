<?php

namespace App\Entity\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\HeadingFirmController;

/**
 * @ApiResource(
 *     shortName="AddHeadingToFirm",
 *     routePrefix="heading",
 *     collectionOperations={
 *         "add_heading"={
 *              "method"="POST", "path"="/firm/add", "controller"=HeadingFirmController::class,
 *              "openapi_context" = {
 *                  "summary" = "Добавление рубрики фирме"
 *                }
 *          }
 *     },
 *     itemOperations={},
 *     output=false
 * )
 */
class AddHeadingFirmResource
{
    /**
     * @var int
     */
    public $firm_id;
    /**
     * @var int
     */
    public $heading_id;
}